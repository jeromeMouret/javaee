<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Java EE</title>
    <style type="text/css">
    	form > div {
    		padding: .5em;
    	}
    	
    	div > label:first-child {
    		display: inline-block;
			min-width: 18em;
		}
		
		.error {
			color: red;
		}
    </style>
  </head>
  <body>

	<form method="post" accept-charset="utf-8">
	
		<div>
			<label for="nom">Nom : </label>
			<input id="nom" name="nom" type="text" value="<c:out value="${param['nom']}" />"> 
			<span class="error"><c:out value="${errors['nom']}"/></span>
		</div>
		<div>
			<label for="prenom">Prenom : </label>
			<input id="prenom" name="prenom" type="text" value="<c:out value="${param['prenom']}" />" >
			<span class="error"><c:out value="${errors['prenom']}"/></span>
		</div>
		
		<div>
			<select name="promo">
			    <c:forEach items="${ inscription }" var="p">
		
		            <option value="<c:out value="${ p }"/>"><c:out value="${ p }"/></option>
		      
				</c:forEach>
			</select>
		</div>
		<div>
			<button type="submit">S'inscrire</button>
		</div>
	</form>

	<div>
	  	<a href="<c:url value="/"/>">Retour à l'accueil</a>  
	</div>
  
  </body>
</html>