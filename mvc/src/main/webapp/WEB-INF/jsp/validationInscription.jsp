<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Java EE</title>
  </head>
  <body>
	<div>Votre inscription a bien été prise en compte le 
		 <fmt:formatDate type="date" dateStyle="long" value="${inscription.date}"/> à 
		 <fmt:formatDate type="time" value="${inscription.date}"/> pour 
		 <c:out value="${inscription.prenom}"/> -
		 <c:out value="${inscription.nom}"/> en 
		 <c:out value="${inscription.promo}"/>.
	</div>
	<div>
	  	<a href="<c:url value="/"/>">Retour à l'accueil</a>  
	</div>
	
	
	<c:forEach items="${ eleve }" var="p">
		<c:if test="${p.promo == param['promo']}">
			<p><c:out value="${ p.prenom }"/>, <c:out value="${ p.nom }"/> : Le <fmt:formatDate type="date" dateStyle="long" value="${inscription.date}"/> à <fmt:formatDate type="time" dateStyle="long" value="${p.date}"/></p>
        </c:if>  
	</c:forEach>
	
	
  </body>
</html>
