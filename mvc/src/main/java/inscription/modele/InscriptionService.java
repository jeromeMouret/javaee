package inscription.modele;

import java.util.ArrayList;
import java.util.List;

public class InscriptionService {
	
	public Inscription inscrire(String nom, String prenom, String promo) throws InscriptionInvalideException {
		InscriptionInvalideException ex = new InscriptionInvalideException();
		
		if (nom == null || nom.length() < 1) {
			ex.addMessage("nom", "Le nom est invalide !");
		}
		if (prenom == null || prenom.length() < 1) {
			ex.addMessage("prenom", "Le prenom est invalide !");
		}
		if (ex.mustBeThrown()) {
			throw ex;
		}
		
		
		return new Inscription(nom, prenom, promo);
	}
	
	public List<String> getList()  {

		List<String> listeAnnee = new ArrayList<>();
		listeAnnee.add("Bachelor niveau 1");
		listeAnnee.add("Bachelor niveau 2");
		listeAnnee.add("Bachelor niveau 3");
		listeAnnee.add("Ingénieur niveau 4");
		listeAnnee.add("Ingénieur niveau 5");
		
		
		return listeAnnee;
	}
	
}
