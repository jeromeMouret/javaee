package inscription.modele;

import java.util.Date;

public class Etudiant {
	
	private String Prenom;
	private String Nom;
	private String promo;
	private Date date;
	
	public Etudiant (String nom, String prenom, String promo, Date date) {
		
		 this.Prenom = prenom;
		 this.Nom = nom;
		 this.promo = promo;
		 this.date = date;
		
	}
	
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPromo() {
		return promo;
	}
	public void setPromo(String promo) {
		this.promo = promo;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	

}
