package inscription.modele;

import java.util.Date;

public class Inscription {

	private String nom;
	private String prenom;
	private Date date;
	private String promo;
	private Etudiant etudiant;
	
	public Inscription(String nom, String prenom, String promo) {
		this.nom = nom;
		this.prenom = prenom;
		this.promo = promo;
		this.date = new Date();
		this.etudiant = new Etudiant(this.nom, this.prenom, this.promo, this.date);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}





}
