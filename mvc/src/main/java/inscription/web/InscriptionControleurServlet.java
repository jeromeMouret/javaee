package inscription.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import inscription.modele.Etudiant;
import inscription.modele.Inscription;
import inscription.modele.InscriptionInvalideException;
import inscription.modele.InscriptionService;

@WebServlet("/inscription")
public class InscriptionControleurServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Etudiant> etudiants = new ArrayList<>();
		getServletContext().setAttribute("eleve", etudiants);
		InscriptionService inscriptionService = new InscriptionService();
		req.setAttribute("inscription", inscriptionService.getList());
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
		rd.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nom = req.getParameter("nom");
		String prenom = req.getParameter("prenom");
		String promo = req.getParameter("promo");

		try {
			InscriptionService inscriptionService = new InscriptionService();
			Inscription inscription = inscriptionService.inscrire(nom, prenom, promo);
			List<Etudiant> eleve = (List<Etudiant>) this.getServletContext().getAttribute("eleve");
			eleve.add(inscription.getEtudiant());
			getServletContext().setAttribute("etudiants", eleve);
			req.setAttribute("inscription", inscription);
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/validationInscription.jsp");
			rd.forward(req, resp);
		} catch (InscriptionInvalideException e) {
			InscriptionService inscriptionServ = new InscriptionService();
			req.setAttribute("errors", e.getMessages());
			req.setAttribute("inscription", inscriptionServ.getList());
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/inscription.jsp");
			rd.forward(req, resp);
		}
	}
}
