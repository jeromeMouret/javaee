<%@page pageEncoding="UTF-8" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <c:set var="nom" value="${param['nom']}"/>
        <c:choose>
            <c:when test="${fn:length(param) eq 0}">
                Vous n'avez envoyé aucun paramètre au serveur !
            </c:when>
            <c:when test="${empty nom}">
                Vous n'avez pas envoyé votre nom au serveur !
            </c:when>
            <c:when test="${fn:startsWith(fn:toLowerCase(nom), 'david')}">
                Tiens ! Vous aussi, vous vous appelez David.
            </c:when>
            <c:otherwise>
                Bonjour ${nom} !
            </c:otherwise>
        </c:choose>
    </body>
</html>