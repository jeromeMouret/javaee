package formulaireMVC;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import formulaireMVC.InscriptionForm;

@WebServlet("/data-check")
public class DataCheckServlet extends HttpServlet {
	
	  @Override
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	                 throws ServletException, IOException {
		  String email = req.getParameter("email");
		  String passwd = req.getParameter("passwd");
		  String passwdcheck = req.getParameter("passwdcheck");
		  String isCheck = req.getParameter("checkBox");
		  
		  InscriptionForm form = new InscriptionForm();
		  Inscription inscription = form.InitInscriptionForm(email, passwd, passwdcheck, isCheck);
		      

		    if ( form.getErreurs().isEmpty() ) {
				  req.setAttribute("singIn", inscription);
				  RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/inscrit.jsp");
				  dispatcher.forward(req, resp);
		    } else {
				  req.setAttribute("erreurs", form.getErreurs());
				  req.setAttribute("email", email);
				  RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
				  dispatcher.forward(req, resp);
		    }

	  }

}