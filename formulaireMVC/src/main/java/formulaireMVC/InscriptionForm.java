package formulaireMVC;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class InscriptionForm {
    private Map<String, String> erreurs = new HashMap<String, String>();


    public Inscription InitInscriptionForm(String email, String mdp, String checkmdp, String isCheck) {
    	Inscription signIn = new Inscription();
	  	try {
		  validationEmail(email);
		  signIn.setEmail(email);
	    } catch ( Exception e ) {
	        setErreur( "email", e.getMessage());
	    }
	  	

	  	try {
	    	validationMotsDePasse( mdp, checkmdp );
	    	signIn.setEmail(mdp);
	    } catch ( Exception e ) {
	    	setErreur( "mdp", e.getMessage() );
	    	setErreur( "passwdcheck", null );
	    }
	  	
	  	
	  	try {
	    	validationCG( isCheck );
	    } catch ( Exception e ) {
	    	setErreur( "cg", e.getMessage() );
	    }
	  	
	  	return signIn;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    
    public void validationEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Merci de saisir une adresse mail valide." );
            }
        } else {
            throw new Exception( "Merci de saisir une adresse mail." );
        }
    }
    
    public void validationCG( String isCheck ) throws Exception {
    	if ( isCheck == null ) {
        	throw new Exception( "Merci de valider les conditions d'utilisation." );
    	}
    }

    public void validationMotsDePasse( String motDePasse, String confirmation ) throws Exception {
        if ( motDePasse != null && confirmation != null ) {
            if ( !motDePasse.equals( confirmation ) ) {
                throw new Exception( "Les mots de passe entrés sont différents, merci de les saisir à nouveau." );
            } else if ( motDePasse.length() < 8 ) {
                throw new Exception( "Les mots de passe doivent contenir au moins 8 caractères." );
            }
        } else {
            throw new Exception( "Merci de saisir et confirmer votre mot de passe." );
        }
    }


    public void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

}
