<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="now" scope="page" class="java.util.Date"/>
        
	<p>
	Votre inscription a bien été prise en compte le <fmt:formatDate type = "date" value = "${now}" />
	à <fmt:formatDate type = "time" value = "${now}" />
	pour l'adresse mail ${singIn.email}.</p>
	
	
</body>
</html>