<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulaire</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>

<p>
  ${erreurs['mdp']}
</p>
<p>
  ${erreurs['email']}
</p>
<p>
  ${erreurs['cg']}
</p>
<hr />

<div class="container">
	<div class="row">
	<div class="col col-lg-3">
	</div>
	<div class="col col-lg-6">
		<form action="./data-check" method="post" accept-charset="utf-8">
		  <div class="form-group row">
		    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="inputEmail3" value="${email}" name="email" placeholder="Email">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="inputPassword3" name="passwd" placeholder="Password">
		    </div>
		  </div>
		   <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-2 col-form-label">Verify password</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="inputPassword3" name="passwdcheck" placeholder="Password">
		    </div>
		  </div>
		  <div class="form-group row">
		    <div class="col-sm-2"></div>
		    <div class="col-sm-10">
		      <div class="form-check">
		        <input class="form-check-input" type="checkbox" name="checkBox" id="gridCheck1">
		        <label class="form-check-label" for="gridCheck1">
		           J’ai lu et approuvé les conditions générales de ce site
		        </label>
		      </div>
		    </div>
		  </div>
		  <div class="form-group row">
		    <div class="col-sm-10">
		      <button type="submit" class="btn btn-primary">Sign in</button>
		    </div>
		  </div>
		</form>
		</div>
		<div class="col col-lg-3">
		</div>
	</div>
</div>

</body>
</html>