package codeBar;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/gen")
public class GenServlet extends HttpServlet{
	
	  @Override
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	                 throws ServletException, IOException {
	    req.setCharacterEncoding("utf-8");
	    String code = req.getParameter("code");
	    String nom = req.getParameter("nom");

	    
	    
	    resp.setContentType("text/html");
	    resp.setCharacterEncoding("utf-8");
	    resp.getWriter().write(nom + " : " );
	    resp.getWriter().write("<img src='codeGen?type=datamatrix&msg="
	    		+ code +
	    		"'/> " );
	  }

}
